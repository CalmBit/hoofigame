﻿using System;
using System.Globalization;

namespace HoofiGame
{
    public enum AilmentType
    {
        BUFF,
        DEBUFF,
        FUN
    }

    public delegate void AilmentTick();

    public delegate void AilmentOnset();

    public delegate void AilmentOffset();

    public class Ailment
    {
        public string Description;
        public string Name;
        public AilmentOffset Offset;
        public AilmentOnset Onset;
        public AilmentTick Tick;
        public int TicksActive;
        public AilmentType Type;

        public Ailment(string name, string description, AilmentType type, AilmentOnset onset, AilmentOffset offset,
            AilmentTick tick = null, int ticksActive = 10)
        {
            Name = name;
            Description = description;
            Type = type;
            TicksActive = ticksActive;
            Onset = onset;
            Offset = offset;
            Tick = tick;
            TicksActive = ticksActive;
        }

        public void PrintAilment(bool withDesc, bool formatted = true)
        {
            if (formatted)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("- ");
            }
            Console.ForegroundColor = Type == AilmentType.BUFF
                ? ConsoleColor.Green
                : Type == AilmentType.DEBUFF ? ConsoleColor.Red : ConsoleColor.Blue;
            if (formatted) Console.WriteLine(Name);
            else Console.Write(Name);
            Console.ForegroundColor = ConsoleColor.White;
            if (withDesc) Console.WriteLine(Description);
            if (formatted)
                Console.WriteLine((withDesc ? "  " : "") + "Turns Left: " +
                                  (TicksActive == -1 ? "Forever" : TicksActive.ToString(CultureInfo.InvariantCulture)));
        }
    }
}