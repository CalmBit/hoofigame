﻿using System;
using System.Collections.Generic;

namespace HoofiGame
{
    class RoomInitalizer
    {

        public static Dictionary<Tuple<int, int>, Room> InitalizeRooms(string playerName)
        {
            return new Dictionary<Tuple<int, int>, Room>
                {
                    {
                        new Tuple<int, int>(0, 0),
                        new Room(playerName + "'s Bedroom",
                            "This is your bedroom. Not much going on here - aside from your messy ass bed.\n\nThere's an open window to your east, and a door to your south.",
                            (int) RoomExits.EAST | (int) RoomExits.SOUTH, RoomType.DECORATIVE)
                    },
                    {
                        new Tuple<int, int>(1, 0),
                        new Room(playerName + "'s Eastern Lawn",
                            "Ten seconds in and you fell out the fucking window.\n\nHmm.\n\nLet's try that again, eh?",
                            0,
                            RoomType.DEATH)
                    },
                    {
                        new Tuple<int, int>(1, -1),
                        new Room(playerName + "'s Bathroom",
                            "A bathroom, with a toilet, sink, and shower.\n\nWest of this is a hallway.",
                            (int) RoomExits.WEST, RoomType.DECORATIVE, new List<Item>
                            {
                                new Item("Sleeping Pill", "A typical Imovane/Zopiclone pill.", () =>
                                {
                                    Console.WriteLine(
                                        "You take the Sleeping Pill - downing it with a cold glass of water.");
                                    Console.WriteLine("Suddenly, you feel horrifcally woozy, as if you can't stand up.");
                                    Program.AddAilment(new Ailment("Zopiclone High", "You're a bit fucked on an OTC hypnotic.",
                                        AilmentType.FUN, () => Console.SetOut(new ZopliconeMixer()),
                                        () => Console.SetOut(Program.StandardOutput)));
                                    Console.WriteLine("Now you're all mixed up!");
                                    Program.RemoveItem("sleeping pill");
                                })
                            })
                    },
                    {
                        new Tuple<int, int>(0, -1),
                        new Room("Hallway",
                            "A hallway.\n\nSouth of this is the main foyer, and to the North is your bedroom. There is also a bathroom to the East.",
                            (int) RoomExits.NORTH | (int) RoomExits.EAST | (int) RoomExits.SOUTH, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(0, -2),
                        new Room("Foyer",
                            "The Foyer is rather expansive, decorated with expensive tiling and lighting.\n\nA grandiose wooden portal to the outside world lies to the South, a Living Room to the East, and a Dining Room to the West.\nTo your north is a hallway.",
                            (int) RoomExits.NORTH | (int) RoomExits.SOUTH | (int) RoomExits.EAST | (int) RoomExits.WEST,
                            RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Key Tree", "A small collection of hooks, usually adorned with various keys.", () => Console.WriteLine("A key tree sits on the wall."), null, new Dictionary<string, Performance>
                                {
                                    {
                                        "attacked",
                                        () =>
                                        {
                                            if (Program.GetNpc("key tree", Program.PlayerPosition).Health == 0)
                                            {
                                                Console.WriteLine("\nYou already clawed at the Key Tree.");
                                                return true;
                                            }
                                            Console.WriteLine("\nYou claw at the Key Tree.");
                                            Program.GetNpc("key tree", Program.PlayerPosition).Health = 0;
                                            var item = new Item("Red House Key",
                                                "A metal key, covered in a chipped red paint.", ItemType.KEY);
                                            Console.Write("By shear luck, a ");
                                            item.PrintItemName(false);
                                            Console.WriteLine(" falls off and onto the floor.");
                                            Program.AddItemToRoom(item,Program.PlayerPosition);
                                            return true;
                                        }
                                    },
                                    {
                                        "speak",
                                        () =>
                                        {
                                            Console.WriteLine("The Key Tree doesn't reply, for it is a Key Tree.");
                                            return true;
                                        }
                                    }
                                })
                            })
                    },
                    {
                        new Tuple<int, int>(1, -2),
                        new Room("Living Room",
                            "A Living Room, furnished with a telivision (barely used).\n\nTo your West is the Foyer.",
                            (int) RoomExits.WEST, RoomType.DECORATIVE,
                            new List<Item>
                            {
                                new Item("Walkman",
                                    "A device for playing your favourite tapes! Now if only people used tapes...",
                                    ItemType.SPECIAL)
                            }, new List<Npc>
                            {
                                new Npc("Couch", "A red couch. Quite fluffy.",
                                    () => Console.WriteLine("A Couch sits idlely by."), null,
                                    new Dictionary<string, Performance>
                                    {
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                if (Program.GetNpc("couch", Program.PlayerPosition).Health == 0)
                                                {
                                                    Console.WriteLine(
                                                        "\nYou've already beat the ever loving shit out of the couch - leave it alone, will ya?");
                                                    return true;
                                                }
                                                Console.WriteLine(
                                                    "\nYou beat the couch with your fist, eventually ripping holes into it.");
                                                Program.GetNpc("couch", Program.PlayerPosition).Health = 0;
                                                var item = new Item("Headphones",
                                                    "Headphones: paired with your Walkman.", ItemType.SPECIAL);
                                                Console.Write("A pair of ");
                                                item.PrintItemName(false);
                                                Console.WriteLine(" drops from one of the cushions.");
                                                Program.AddItemToRoom(item,Program.PlayerPosition);
                                                return true;
                                            }
                                        },
                                        {
                                            "speak",
                                            () =>
                                            {
                                                Console.WriteLine("\nThe couch says nothing, for it is a couch.");
                                                return true;
                                            }
                                        }
                                    })
                            })
                    },
                    {
                        new Tuple<int, int>(-1, -2),
                        new Room("Dining Room",
                            "A Dining Room, with a table and various candles.\nSeems to be set up for a breakfast, but god knows where anyone else is..\n\nTo your East is the Foyer.",
                            (int) RoomExits.EAST, RoomType.DECORATIVE, 
                            new List<Item>
                            {
                                new Item("Salt Shaker", "A container full of salt.", ItemType.INGREDIENT, () => Console.WriteLine("\nYou put some salt on your tongue.\nYou wince - that's disgusting!"))
                            },
                            new List<Npc>
                            {
                                new Npc("Refrigerator", "A metal receptacle for the storage of cold goods.", 
                                    () => Console.WriteLine("A refrigerator stands motionless."),
                                    null,
                                    new Dictionary<string, Performance>
                                    {
                                        {
                                            "speak",
                                            () =>
                                            {
                                                Console.WriteLine(
                                                    "\nThe Refrigerator doesn't reply, for it is a Refrigator.");
                                                return true;
                                            }
                                        },
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                if (Program.GetNpc("refrigerator", Program.PlayerPosition).Health == 0)
                                                {
                                                    Console.WriteLine("\nYou already clawed at the refrigerator - no sense in doing that again.");
                                                    return true;
                                                }
                                                Console.WriteLine("\nYou desperately claw at the refigrator like a hungry cat.\nBy some twist of fate, you manage to open it.");
                                                Program.GetNpc("refrigerator", Program.PlayerPosition).Health = 0;
                                                var milk = new Item("Milk Jug",
                                                    "A half-gallon jug of 1% milk. Mooth-Brothers Dairy!",
                                                    ItemType.INGREDIENT,
                                                    () =>
                                                        Console.WriteLine(
                                                            "\nYou take a small swig of milk straight out of the jug.\nLack of manners aside, it tastes pretty good."));
                                                Console.Write("\nA ");
                                                milk.PrintItemName(false);
                                                Console.WriteLine(" falls out, but it escapes unharmed.");
                                                Program.AddItemToRoom(milk,Program.PlayerPosition);
                                                return true;
                                            }
                                        }
                                    })
                            })
                    },
                    {
                        new Tuple<int, int>(0, -3),
                        new Room(playerName + "'s Front Lawn",
                            "The air outside your house is crisp and clear.\nYou take a deep breath, and almost swallow a bug.\n\nTo your North is the door to your house, and to your South there's a dirt road.",
                            (int) RoomExits.NORTH | (int) RoomExits.SOUTH, RoomType.DECORATIVE)
                    },
                    {
                        new Tuple<int, int>(0, -4),
                        new Room("Dirt Road",
                            "A dirt road, freshly grated and torn through by car tires.\n\nTo the West and East, the road continues.\nTo the North lies your house.",
                            (int) RoomExits.NORTH | (int) RoomExits.WEST | (int) RoomExits.EAST, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(1, -4),
                        new Room("Dirt Road",
                            "A dirt road, freshly grated and torn through by car tires.\n\nTo the West and East, the road continues.\nTo the South, a small path leads to a large pyramid. When did that get here?",
                            (int) RoomExits.SOUTH | (int) RoomExits.WEST | (int) RoomExits.EAST, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(2, -4),
                        new Room("Dirt Road",
                            "A dirt road, freshly grated and torn through by car tires.\n\nTo the West and East, the road continues.",
                            (int) RoomExits.WEST | (int) RoomExits.EAST, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(3, -4),
                        new Room("Dirt Road",
                            "A dirt road, freshly grated and torn through by car tires.\n\nTo the West and South, the road continues.\nTo the North is an unassuming red house.",
                            (int) RoomExits.NORTH | (int) RoomExits.WEST | (int) RoomExits.SOUTH,
                            RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(3, -5),
                        new Room("Dirt Road",
                            "A dirt road, freshly grated and torn through by car tires.\n\nTo the North and South, the road continues.",
                            (int) RoomExits.NORTH | (int) RoomExits.SOUTH, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(3, -6),
                        new Room("Dirt Road",
                            "A dirt road, freshly grated and torn through by car tires.\n\nTo the North and South, the road continues.\nTo the East is an unassuming blue house.",
                            (int) RoomExits.NORTH | (int) RoomExits.SOUTH | (int) RoomExits.EAST, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(4, -6),
                        new Room("Blue House Front Lawn",
                            "A wood paneled house, covered in blue paint. The windows are ornate, displaying the owner's wealth.\n\nTo the West there's a dirt road, and to the East lies a door.",
                            (int) RoomExits.WEST | (int) RoomExits.EAST, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Door",
                                    "A reinforced metal door. It seems to be locked - you might need a key to enter.",
                                    null, null, new Dictionary<string, Performance>
                                    {
                                        {
                                            "go_east",
                                            () =>
                                            {
                                                if (Program.PlayerHasItem("blue house key"))
                                                {
                                                    Console.Write("\nYou insert the ");
                                                    Program.GetItemFromInventory("blue house key").PrintItemName(false);
                                                    Console.WriteLine(
                                                        " into the keyhole, and turn it.\nWhen you turn the knob, the Door opens, allowing you to pass.\n");
                                                    return true;
                                                }
                                                Console.WriteLine(
                                                    "\nYou jiggle the knob, but it doesn't open. Maybe you need a key?");
                                                return false;
                                            }
                                        },
                                        {
                                            "speak",
                                            () =>
                                            {
                                                Console.WriteLine("The door doesn't reply, for it is a door.");
                                                return true;
                                            }
                                        },
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                Console.WriteLine(
                                                    "\nYou punch the metal door. Nothing of interest occurs, besides your hand hurting.");
                                                return true;
                                            }
                                        }
                                    }),
                                    new Npc("Doormat", "An aluminium mat.", () => Console.WriteLine("A Doormat sits underneath the door."), null, new Dictionary<string, Performance>
                                    {
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                if (Program.GetNpc("doormat", Program.PlayerPosition).Health == 0)
                                                {
                                                    Console.WriteLine("\nYou already peeled back the doormat.");
                                                    return true;
                                                }
                                                Console.WriteLine("\nYou peel back the doormat, revealing the concrete steps beneath.");
                                                Program.GetNpc("doormat", Program.PlayerPosition).Health = 0;
                                                var key = new Item("Blue House Key",
                                                    "A metal key, covered with a pearlescent blue paint.", ItemType.KEY);
                                                Console.Write("A ");
                                                key.PrintItemName(false);
                                                Console.WriteLine(" is on the step beneath the mat.\nPoor planning on the homeowners part.");
                                                Program.AddItemToRoom(key,Program.PlayerPosition);
                                                return true;
                                            }
                                        }
                                    })
                            })
                    },
                    {
                        new Tuple<int, int>(5, -6),
                        new Room("Blue House Foyer", "A grandiose area, the foyer is well lit and decorated.\nAssorted statues clutter the area, and various impressionist paintings\nhang on the wall.\n\nTo your West, there's a door to the outside world.\nTo your North lies a Bedroom, and to the South a kitchen.",
                            (int)RoomExits.NORTH | (int)RoomExits.SOUTH | (int)RoomExits.WEST, RoomType.DECORATIVE)
                    },
                    {
                        new Tuple<int, int>(5, -5),
                        new Room("Blue House Bedroom", "A beautifully decorated master bedroom, adorned with various light fixtures.\n\nTo the South is the house's foyer.", 
                            (int)RoomExits.SOUTH, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Bureau", "An ornate wooden Bureau.", () => Console.WriteLine("A bureau sits at one end of the room."), null, new Dictionary<string, Performance>
                                {
                                    {
                                        "attacked",
                                        () =>
                                        {
                                            if (Program.GetNpc("bureau", Program.PlayerPosition).Health == 0)
                                            {
                                                Console.WriteLine("\nYou've already opened the bureau.");
                                                return true;
                                            }
                                            Console.WriteLine("\nYou claw at the bureau, and manage to hook one of the drawers.");
                                            Console.WriteLine("It clatters to the ground, smashing to pieces.");
                                            var item = new Item("Dildo", "...", ItemType.EASTER,
                                                () =>
                                                {
                                                    if (playerName.ToLower() == "hoofi" ||
                                                        playerName.ToLower() == "christie")
                                                        Console.WriteLine("\n" + playerName +
                                                                          ", you know better than that!");
                                                    else
                                                        Console.WriteLine(
                                                            "\nWhat type of fucking game do you think this is?");
                                                });
                                            Console.Write("\nA ");
                                            item.PrintItemName(false);
                                            Console.WriteLine(" is in the wreckage...");
                                            Console.WriteLine("\n...even the rich have needs.");
                                            Program.AddItemToRoom(item,Program.PlayerPosition);
                                            return true;
                                        }
                                    }
                                })
                            })
                    },
                    {
                        new Tuple<int, int>(5,-7),
                        new Room("Blue House Kitchen", "A beautiful kitchen, complete with up to date appliances and light fixtures.\n\nTo the North is the house's foyer.",
                            (int)RoomExits.NORTH, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Refrigerator", "A metal receptacle for the storage of cold goods.", () => Console.WriteLine("A refrigerator sits and drones at one of the walls."), null, new Dictionary<string, Performance>
                                {
                                    {
                                        "speak",
                                        () =>
                                        {
                                            Console.WriteLine("\nThe refrigerator does not reply, for it is a refrigerator.");
                                            return true;
                                        }
                                    },
                                    {
                                        "attacked",
                                        () =>
                                        {
                                            if (Program.GetNpc("refrigerator", Program.PlayerPosition).Health == 0)
                                            {
                                                Console.WriteLine("\nYou've already opened the regfrigerator.");
                                                return true;
                                            }
                                            Console.WriteLine("\nYou desperately claw at the refigrator like a hungry cat.\nBy some twist of fate, you manage to open it.");
                                            Program.GetNpc("refrigerator", Program.PlayerPosition).Health = 0;
                                            var eggs = new Item("Egg Carton",
                                                "A carton of fresh eggs - apparently free range.", ItemType.INGREDIENT);
                                            Console.Write("An ");
                                            eggs.PrintItemName(false);
                                            Console.WriteLine(" falls to the ground, miraculously staying in one piece.");
                                            Program.AddItemToRoom(eggs,Program.PlayerPosition);
                                            return true;
                                        }
                                    }
                                })
                            })
                    },
                    {
                        new Tuple<int, int>(3, -3),
                        new Room("Red House Front Lawn",
                            "A wood paneled house, covered in red paint.\nThe windows are broken, letting in the various elements.\n\nTo the South there's a dirt road, and to the North lies a door.",
                            (int) RoomExits.NORTH | (int) RoomExits.SOUTH, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Door",
                                    "A basic wooden door. It seems to be locked - you might need a key to enter.", null,
                                    null, new Dictionary<string, Performance>
                                    {
                                        {
                                            "go_north",
                                            () =>
                                            {
                                                if (Program.PlayerHasItem("red house key"))
                                                {
                                                    Console.Write("\nYou insert the ");
                                                    Program.GetItemFromInventory("red house key").PrintItemName(false);
                                                    Console.WriteLine(
                                                        " into the keyhole, and turn it.\nWhen you turn the knob, the Door opens, allowing you to pass.\n");
                                                    return true;
                                                }
                                                Console.WriteLine(
                                                    "\nYou jiggle the knob, but it doesn't open. Maybe you need a key?");
                                                return false;
                                            }
                                        },
                                        {
                                            "speak",
                                            () =>
                                            {
                                                Console.WriteLine("\nThe door doesn't reply, for it is a door.");
                                                return true;
                                            }
                                        },
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                if (Program.PlayerHasAilment("splinter"))
                                                {
                                                    Console.WriteLine(
                                                        "\nYou're not gonna try that again - not after that splinter.");
                                                    return true;
                                                }
                                                Console.WriteLine(
                                                    "\nYou punch the door repeatedly, but it doesn't seem to budge.");
                                                Program.AddAilment(new Ailment("Splinter",
                                                    "You managed to get a splinter of wood lodged in your body.\nGood luck getting that out.",
                                                    AilmentType.DEBUFF,
                                                    () =>
                                                    {
                                                        Console.WriteLine(
                                                            "A splinter of wood has lodged itself in your body.");
                                                        Program.PlayerHealth--;
                                                    },
                                                    () =>
                                                        Console.WriteLine(
                                                            "The splinter finally falls away, but the afflicted area still hurts."),
                                                    () =>
                                                    {
                                                        if (Program.Random.Next(20) != 6) return;
                                                        Console.WriteLine("\nYou're hurt by your splinter!");
                                                        Program.PlayerHealth--;
                                                    }, 30));
                                                return true;
                                            }
                                        }
                                    })
                            })
                    },
                    {
                        new Tuple<int, int>(3, -2),
                        new Room("Red House Foyer",
                            "Leaves clutter the inside of the house.\nIt looks like at one point this could have been a very beautiful interior,\nbut thanks to the elements, most of the colours have faded.\n\nTo your South, there's a door to the outside world.\nTo the East lies a bedroom, and to the West is a dining room.",
                            (int) RoomExits.SOUTH | (int) RoomExits.EAST | (int) RoomExits.WEST, RoomType.DECORATIVE)
                    },
                    {
                        new Tuple<int, int>(2, -2),
                        new Room("Red House Dining Room",
                            "The dining room of the house is nearly pristine - odd\ngiven its clear state of disrepair.\n\nTo the East is the house's Foyer.",
                            (int) RoomExits.EAST, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Cabinet", "A wooden cabinet.",
                                    () => Console.WriteLine("A seemingly stocked cabinet hangs above the counter."),
                                    null, new Dictionary<string, Performance>
                                    {
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                if (Program.GetNpc("cabinet", Program.PlayerPosition).Health == 0)
                                                {
                                                    Console.WriteLine("\nYou've already beat up the cabinet.");
                                                    return true;
                                                }
                                                var flour = new Item("Flour Bag", "A bag of white flour.",
                                                    ItemType.INGREDIENT);
                                                Console.WriteLine(
                                                    "\nYou smash your fists into the wooden paneling of the cabinet.\nIt gives way fairly quickly.");
                                                Console.Write("A ");
                                                flour.PrintItemName(false);
                                                Console.WriteLine(" falls to the ground, but doesn't spill.");
                                                Program.AddItemToRoom(flour,Program.PlayerPosition);
                                                return true;
                                            }
                                        }
                                    })
                            })
                    },
                    {
                        new Tuple<int, int>(4, -2),
                        new Room("Red House Bedroom",
                            "A bedroom, with a broken down bed and strewn sheets, with a ripped pillow topping it.\n\nTo the West is the house's Foyer.",
                            (int) RoomExits.WEST, RoomType.DECORATIVE, new List<Item>
                            {
                                new Item("Nohj's 101 Binural Beats",
                                    "The packaging reads \"101 of your favourite binural hits.\"\nA small disclaimer at the bottom reads \"Most claims unverified/probably bullshit.\"",
                                    ItemType.TAPE,
                                    () =>
                                    {
                                        if (Program.PlayerHasItem("walkman"))
                                        {
                                            Console.WriteLine("\nYou put the tape into the deck.");
                                            if (Program.PlayerHasItem("headphones"))
                                            {
                                                Console.WriteLine(
                                                    "\nTwo tones can be heard - one in each ear, both differing by as little as a few hertz.");
                                                Program.AddAilment(new Ailment("Binurally Beat",
                                                    "You feel at ease, one with the universe.", AilmentType.FUN,
                                                    () =>
                                                    {
                                                        Console.ForegroundColor = ConsoleColor.Cyan;
                                                        Console.WriteLine("You begin to feel at peace.");
                                                        Console.ForegroundColor = ConsoleColor.White;
                                                    },
                                                    () => Console.WriteLine("The feeling of relaxation fades."), null,
                                                    30));
                                                return;
                                            }
                                            Console.WriteLine("\nYou hear weird tones, and think nothing of it.");
                                            return;
                                        }
                                        Console.WriteLine("\nYou would play this...if you had a tape player.");
                                    })
                            })
                    },
                    {
                        new Tuple<int, int>(-1, -4),
                        new Room("Dirt Road",
                            "A dirt road, freshly grated and torn through by car tires.\n\nTo the West and East, the road continues.",
                            (int) RoomExits.WEST | (int) RoomExits.EAST, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(3, -7), 
                        new Room("Dirt Road",
                            "A dirt road, freshly grated and torn through by car tires.\n\nTo the North and South, the road continues.",
                            (int)RoomExits.NORTH | (int)RoomExits.SOUTH, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(3, -8),
                        new Room("Dirt Road",
                            "A dirt road, freshly grated and torn through by car tires.\n\nTo The North and South, the road continues.",
                            (int)RoomExits.NORTH | (int)RoomExits.SOUTH, RoomType.PATH, new List<Npc>
                            {
                                new Npc("Dirt Pile", "A dirty pile of dirt.", () => Console.WriteLine("A raised pile of dirt is on the path."), null, new Dictionary<string, Performance>
                                {
                                    {
                                        "speak",
                                        () =>
                                        {
                                            Console.WriteLine("\nThe dirt pile says nothing, for it is a dirt pile.");
                                            return true;
                                        }
                                    },
                                    {
                                        "attacked",
                                        () =>
                                        {
                                            if (Program.GetNpc("dirt pile", Program.PlayerPosition).Health == 0)
                                            {
                                                Console.WriteLine("\nYou already decimated the pile.");
                                                return true;
                                            }
                                            Console.WriteLine("\nYou claw at the dirt pile, ripping it up.");
                                            Program.GetNpc("dirt pile", Program.PlayerPosition).Health = 0;
                                            var plate = new Item("Plate", "A fine ceramic plate, with various adornments. A little bit dirty.",
                                                ItemType.INGREDIENT, () => Console.WriteLine("\nYou bite into the plate.\nIt hurts your teeth."));
                                            Console.Write("A ");
                                            plate.PrintItemName(false);
                                            Console.WriteLine(" is left in the wreckage.");
                                            Program.AddItemToRoom(plate,Program.PlayerPosition);
                                            return true;
                                        }
                                    }
                                })
                            })
                    },
                    {
                        new Tuple<int, int>(3, -9),
                        new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the North and West, the road continues.",
                                (int)RoomExits.NORTH | (int)RoomExits.WEST, RoomType.PATH)
                    },
                    {
                      new Tuple<int, int>(2, -9),
                        new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the West and East, the road continues.\nTo the South is a bizzare green house.",
                                (int)RoomExits.WEST | (int)RoomExits.EAST | (int)RoomExits.SOUTH, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(1, -9),
                         new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the West and East, the road continues.",
                                (int)RoomExits.WEST | (int)RoomExits.EAST, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(0, -9),
                         new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the West and East, the road continues.",
                                (int)RoomExits.WEST | (int)RoomExits.EAST, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(-1, -9),
                         new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the West and East, the road continues.",
                                (int)RoomExits.WEST | (int)RoomExits.EAST, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(-2, -9),
                         new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the East and North, the road continues.\nTo the South is her house.",
                                (int)RoomExits.EAST | (int)RoomExits.NORTH | (int)RoomExits.SOUTH, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(-2, -8),
                         new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the North and South, the road continues.",
                                (int)RoomExits.SOUTH | (int)RoomExits.NORTH, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(-2, -7),
                         new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the North and South, the road continues.\nTo the West is an unassuming yellow house.",
                                (int)RoomExits.SOUTH | (int)RoomExits.NORTH | (int)RoomExits.WEST, RoomType.PATH)
                    }, 
                    {
                        new Tuple<int, int>(-2, -6),
                         new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the North and South, the road continues.",
                                (int)RoomExits.SOUTH | (int)RoomExits.NORTH, RoomType.PATH, new List<Item>
                                {
                                    new Item("Paper", "A piece of paper with some writing on it.", ItemType.SPECIAL,
                                        () =>
                                        {
                                            Console.Clear();
                                            Console.ForegroundColor = ConsoleColor.Blue;
                                            Console.WriteLine("Spongecake Recipe:");
                                            Console.ForegroundColor = ConsoleColor.Gray;
                                            Console.WriteLine("\n1. Mix milk and butter.\n\n2. Mix eggs into milk and butter.\n\n3. Mix flour and salt.\n\n4. Mix baking soda into flour and salt.\n\n5. Mix two mixtures together.\n\n6. Mix in vanilla.\n\n7. Bake!\n\n8. ???\n\n9. Profit.");
                                            Program.PressAnyKey();
                                            Console.Clear();
                                        })
                                })
                    },
                    {
                        new Tuple<int, int>(-2, -5),
                         new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the North and South, the road continues.",
                                (int)RoomExits.SOUTH | (int)RoomExits.NORTH, RoomType.PATH)
                    },
                    {
                        new Tuple<int, int>(-2, -4),
                         new Room("Dirt Road",
                                "A dirt road, freshly grated and torn through by car tires.\n\nTo the South and East, the road continues.\nTo the West is the coal processing plant.",
                                (int)RoomExits.SOUTH | (int)RoomExits.EAST | (int)RoomExits.WEST, RoomType.PATH)
                    },
                    {
                      new Tuple<int, int>(2, -10),
                      new Room("Green House Lawn", 
                              "A wood paneled house, covered in green paint. The windows are surreal, multi-coloured.\n\nTo the North there's a dirt road, and to the South lies a door.",
                              (int) RoomExits.NORTH | (int) RoomExits.SOUTH , RoomType.DECORATIVE, new List<Npc>
                              {
                                  new Npc("Door", "A reinforced metal door. It seems to be locked - you might need a key to enter.", null, null, new Dictionary<string, Performance>
                                    {
                                        {
                                            "go_south",
                                            () =>
                                            {
                                                if (Program.PlayerHasItem("green house key"))
                                                {
                                                    Console.Write("\nYou insert the ");
                                                    Program.GetItemFromInventory("green house key").PrintItemName(false);
                                                    Console.WriteLine(
                                                        " into the keyhole, and turn it.\nWhen you turn the knob, the Door opens, allowing you to pass.\n");
                                                    return true;
                                                }
                                                Console.WriteLine(
                                                    "\nYou jiggle the knob, but it doesn't open. Maybe you need a key?");
                                                return false;
                                            }
                                        },
                                        {
                                            "speak",
                                            () =>
                                            {
                                                Console.WriteLine("The door doesn't reply, for it is a door.");
                                                return true;
                                            }
                                        },
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                Console.WriteLine(
                                                    "\nYou punch the metal door. Nothing of interest occurs, besides your hand hurting.");
                                                return true;
                                            }
                                        }
                                    }),
                                    new Npc("Dog", "A beautiful saluki, dyed a deep blue. On it's collar is a key of some sort.", ()=>Console.WriteLine((Program.GetNpc("dog",Program.PlayerPosition).Health != 0 ? "A dog sits longily on the ground." : "A dog corpse sits, crumpled.")), null, new Dictionary<string, Performance>
                                    {
                                        {
                                            "speak",
                                            () =>
                                            {
                                                Console.WriteLine("\nThe dog barks at you.");
                                                return true;
                                            }
                                        },
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                if (Program.GetNpc("dog", Program.PlayerPosition).Health == 0)
                                                {
                                                    Console.WriteLine("\nYou've already killed the dog, you fuck.");
                                                    return true;
                                                }
                                                Console.WriteLine("\nYou punch the dog in the face.\nIt's incredibly fatal.");
                                                Program.GetNpc("dog", Program.PlayerPosition).Health = 0;
                                                var key = new Item("Green House Key",
                                                    "A metal key, covered with a reflective green paint.", ItemType.KEY);
                                                Console.Write("A ");
                                                key.PrintItemName(false);
                                                Console.WriteLine(" drops from its collar.");
                                                Program.AddItemToRoom(key,Program.PlayerPosition);
                                                return true;
                                            }
                                        }
                                    })
                              })
                    },
                    {
                        new Tuple<int, int>(2, -11),
                        new Room("Green House Foyer", "The foyer is covered in statues of varied size, all incredibly strange and contorted.\n\nTo the North is a portal to the outside world.\nTo the West lies a bedroom, and to the East there's a kitchen.", 
                            (int)RoomExits.NORTH | (int)RoomExits.WEST | (int)RoomExits.EAST, RoomType.DECORATIVE)
                    },
                    {
                        new Tuple<int, int>(1, -11),
                        new Room("Green House Bedroom", "The bedroom is adorned with various paintings of faces and colours.\nThey're rather unsettling to look at.\n\nTo the East is the house's foyer.",
                            (int)RoomExits.EAST, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Calendar", "A calendar. Each picture on every month is of a Turtle.\nEvery day is marked as Tuesday.\nYou're intensely worried.", ()=>Console.WriteLine("A weird looking calendar hangs on the wall."))
                            })
                    },
                    {
                        new Tuple<int, int>(3, -11),
                        new Room("Green House Kitchen", "The kitchen has various appliances in weird, neon-bright colours.\n\nTo the West is the house's foyer.",
                            (int)RoomExits.WEST, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Cabinet", "A lime-green coloured wooden cabinet.", ()=>Console.WriteLine("A cabinet hangs above a sink."), null, new Dictionary<string, Performance>
                                {
                                    {
                                        "attacked",
                                        () =>
                                        {
                                            if (Program.GetNpc("cabinet", Program.PlayerPosition).Health == 0)
                                            {
                                                Console.WriteLine("\nYou already opened the cabinet.");
                                                return true;
                                            }
                                            Console.WriteLine("\nYou claw at the cabinet, and manage to hook onto the handle.");
                                            Program.GetNpc("cabinet", Program.PlayerPosition).Health = 0;
                                            var powder = new Item("Baking Powder Can",
                                                "A can of baking powder. Powdery!", ItemType.INGREDIENT,
                                                () =>
                                                    Console.WriteLine(
                                                        "\nYou take a taste of the baking powder.\nIt tastes horrible."));
                                            Console.Write("A ");
                                            powder.PrintItemName(false);
                                            Console.WriteLine(" falls to the ground.");
                                            Program.AddItemToRoom(powder, Program.PlayerPosition);
                                            return true;
                                        }
                                    }

                                })
                            })
                    },
                    {
                      new Tuple<int, int>(-3, -7),
                      new Room("Yellow House Lawn", 
                              "A wood paneled house, covered in yellow paint. The windows are...odd - \noval shapes with what can only be described as a pupil. \"Cho Kawaii?\"\n\nTo the East, there's a dirt road, and to the West lies a door.",
                              (int) RoomExits.WEST | (int)RoomExits.EAST , RoomType.DECORATIVE, new List<Npc>
                              {
                                  new Npc("Door", "A reinforced metal door. It seems to be locked - you might need a key to enter.", null, null, new Dictionary<string, Performance>
                                    {
                                        {
                                            "go_south",
                                            () =>
                                            {
                                                if (Program.PlayerHasItem("yellow house key"))
                                                {
                                                    Console.Write("\nYou insert the ");
                                                    Program.GetItemFromInventory("yellow house key").PrintItemName(false);
                                                    Console.WriteLine(
                                                        " into the keyhole, and turn it.\nWhen you turn the knob, the Door opens, allowing you to pass.\n");
                                                    return true;
                                                }
                                                Console.WriteLine(
                                                    "\nYou jiggle the knob, but it doesn't open. Maybe you need a key?");
                                                return false;
                                            }
                                        },
                                        {
                                            "speak",
                                            () =>
                                            {
                                                Console.WriteLine("The door doesn't reply, for it is a door.");
                                                return true;
                                            }
                                        },
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                Console.WriteLine(
                                                    "\nYou punch the metal door. Nothing of interest occurs, besides your hand hurting.");
                                                return true;
                                            }
                                        }
                                    }),
                                    new Npc("Mailbox", "A metal mailbox, left slightly ajar.", ()=>Console.WriteLine("A metal mailbox is near the edge of the yard."), null, new Dictionary<string, Performance>
                                    {
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                if (Program.GetNpc("mailbox", Program.PlayerPosition).Health == 0)
                                                {
                                                    Console.WriteLine("\nYou've already opened the mailbox.");
                                                    return true;
                                                }
                                                Console.WriteLine("\nYou claw at the mailbox, managing to pry it open further.");
                                                Program.GetNpc("mailbox", Program.PlayerPosition).Health = 0;
                                                Console.Write("Along with a ");
                                                var magazine = new Item("Hentai Magazine",
                                                    "Full of anime porn. The title boasts \"Triple X UNCENSORED ANIME BABES.\"",
                                                    ItemType.EASTER,
                                                    () =>
                                                    {
                                                        Console.WriteLine("\nAs you wish...");
                                                        System.Diagnostics.Process.Start("http://reddit.com/r/hentai");
                                                    });
                                                magazine.PrintItemName(false);
                                                Console.Write(", a ");
                                                var key = new Item("Yellow House Key",
                                                    "A metal key, covered in yellow paint. It's also got an anime sticker of some sort.",
                                                    ItemType.KEY);
                                                key.PrintItemName(false);
                                                Console.WriteLine(" drops onto the floor.");
                                                Program.AddItemToRoom(magazine, Program.PlayerPosition);
                                                Program.AddItemToRoom(key, Program.PlayerPosition);
                                                return true;
                                            }
                                        }
                                    })
                              })
                    },
                    {
                        new Tuple<int, int>(-4, -7), 
                        new Room("Yellow House Foyer", "The foyer is covered in various posters of anime girls.\nThe dedication is unsettling.\n\nTo the North is bedroom, and to the South there's a dining room.\nTo the East is a portal to the outside world.",
                            (int)RoomExits.NORTH | (int)RoomExits.SOUTH | (int)RoomExits.EAST,RoomType.DECORATIVE)
                    },
                    {
                        new Tuple<int, int>(-4, -6),
                        new Room("Yellow House Bedroom", "The bedroom is a mess - tissues litter the floor, some used.\nTwo empty bottles of lotion are near a computer desk.\n\nTo the South is the house's foyer.",
                            (int)RoomExits.SOUTH, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Poster", "An out of place poster.", () => Console.WriteLine(Program.RoomHasNpc("poster", Program.PlayerPosition)
                                    ? "A poster that seems rather out of place hangs on the wall."
                                    : "A safe hangs on the wall. Maybe if you can crack the code..."), null, new Dictionary<string, Performance>
                                    {
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                if (Program.RoomHasNpc("poster", Program.PlayerPosition))
                                                {
                                                    Console.WriteLine("\nYou rip the poster apart - a poor waste of perfect anime tits.\n\nBeneath the poster is a safe!");
                                                    Program.GetNpc("poster", Program.PlayerPosition).Description =
                                                        "A safe - what could it hold?";
                                                    Program.GetNpc("poster", Program.PlayerPosition).Name = "Safe";
                                                    return true;
                                                }
                                                if (Program.GetNpc("safe", Program.PlayerPosition).Health == 0)
                                                {
                                                    Console.WriteLine("\nYou've already opened up the safe.");
                                                    return true;
                                                }
                                                Console.Write("\nEnter the four digit code: ");
                                                var code = Console.ReadLine();
                                                if (code == "3423")
                                                {
                                                    Console.WriteLine("\nThe safe beeps, and cracks open!");
                                                    Program.GetNpc("safe", Program.PlayerPosition).Health = 0;
                                                    var vanilla = new Item("Vanilla Extract",
                                                        "A bottle of vanilla extract.", ItemType.INGREDIENT,
                                                        () =>
                                                            Console.WriteLine(
                                                                "\nYou take a tiny taste of the vanilla. It's very good!"));
                                                    Console.Write("A bottle of ");
                                                    vanilla.PrintItemName(false);
                                                    Console.WriteLine(" falls to the ground.");
                                                    Program.AddItemToRoom(vanilla, Program.PlayerPosition);
                                                    return true;
                                                }
                                                Console.WriteLine("\nThe safe beeps, and doesn't open.");
                                                return true;
                                            }
                                        }
                                    })
                            })
                    },
                    {
                        new Tuple<int, int>(-4, -8),
                        new Room("Yellow House Dining Room", "A dining room. It doesn't look like it's been used in years.\n\nTo the North is the house's foyer.",
                            (int)RoomExits.NORTH, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Microwave", "A microwave, shaped like Totoro. The timer reads \"3423\".", ()=>Console.WriteLine("A microwave sits on a counter.")),
                                new Npc("Refrigerator", "A metal receptacle for the storage of cold goods.", 
                                    () => Console.WriteLine("A refrigerator stands motionless."),
                                    null,
                                    new Dictionary<string, Performance>
                                    {
                                        {
                                            "speak",
                                            () =>
                                            {
                                                Console.WriteLine(
                                                    "\nThe Refrigerator doesn't reply, for it is a Refrigator.");
                                                return true;
                                            }
                                        },
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                if (Program.GetNpc("refrigerator", Program.PlayerPosition).Health == 0)
                                                {
                                                    Console.WriteLine("\nYou already clawed at the refrigerator - no sense in doing that again.");
                                                    return true;
                                                }
                                                Console.WriteLine("\nYou desperately claw at the refigrator like a hungry cat.\nBy some twist of fate, you manage to open it.");
                                                Program.GetNpc("refrigerator", Program.PlayerPosition).Health = 0;
                                                var butter = new Item("Stick of Butter",
                                                    "A whole stick of butter.",
                                                    ItemType.INGREDIENT);
                                                Console.Write("\nA ");
                                                butter.PrintItemName(false);
                                                Console.WriteLine(" falls out.");
                                                Program.AddItemToRoom(butter,Program.PlayerPosition);
                                                return true;
                                            }
                                        }
                                    })
                            })
                    },
                    {
                        new Tuple<int, int>(1, -5),
                        new Room("Mysterious Pyramid",
                            "A weird structure, seemingly composed of a brassy metal.\nThe sounds of chanting can be heard from the inside.\n\nTo the North lies a dirt road, and a door to the Pyramid is in the South.",
                            (int) RoomExits.NORTH | (int) RoomExits.SOUTH, RoomType.DECORATIVE,
                            new List<Npc>
                            {
                                new Npc("Pyramid Templar",
                                    "A hooded figure, standing in front of the pyramid door.\nHe stands motionless, as if he's devoid of a soul.",
                                    () => Console.WriteLine("\nThe Pyramid Templar emits a forebording aura.\n"),
                                    () => Console.WriteLine("\nThe Pyramid Templar wishes you godspeed.\n"),
                                    new Dictionary<string, Performance>
                                    {
                                        {
                                            "go_south",
                                            () =>
                                            {
                                                Console.WriteLine(
                                                    "\nThe Pyramid Templar jumps to attention: \"HALT! Who goes there?\"");
                                                Console.WriteLine("\nHe looks you over.");
                                                if (playerName.ToLower() == "christie" ||
                                                    playerName.ToLower() == "hoofi" ||
                                                    playerName.ToLower() == "{debug}")
                                                {
                                                    Console.WriteLine("\nCould it be? Ms. " + playerName +
                                                                      ", spacu range of legend?");
                                                    Console.WriteLine(
                                                        "Come in, please! Grace our temple with your presence!\n");
                                                    return true;
                                                }
                                                Console.WriteLine(
                                                    "\n\"You're not permitted here - only those truly holy may enter.\"\n");
                                                return false;
                                            }
                                        },
                                        {
                                            "speak",
                                            () =>
                                            {
                                                Console.WriteLine(
                                                    "\nThe Pyramid Templar stands motionless.\nHe doesn't seem to be in the mood to talk.\n");
                                                return true;
                                            }
                                        },
                                        {
                                            "attacked",
                                            () =>
                                            {
                                                Console.WriteLine(
                                                    "\nYou claw desperately at the Pyramid Templar, but he quickly dispatches you with a sword concealed in his robes.\n");
                                                Program.PlayerHealth = 0;
                                                return true;
                                            }
                                        }
                                    })
                            })
                    },
                    {
                        new Tuple<int, int>(1, -6),
                        new Room("Pyramid Floor",
                            "The inside of the Pyramid is coated with lush velvet carpeting, and various skulls lie on brassy cabinets - all adorned with jewels.\n\nTo your North lies a door to the outside world.",
                            (int) RoomExits.NORTH, RoomType.DECORATIVE,
                            new List<Npc>
                            {
                                new Npc("Skeltal Altar",
                                    "dis is gud altar, be vry sur.\nis nice n stufs.\ngold n shinny 4 mr. skeltal.",
                                    () =>
                                        Console.WriteLine(
                                            "The Skeltal Altar glows with a memey shine. You're intensely worried.\nYou wonder what will happen if you're crazy enough to...speak...to it."),
                                    () =>
                                    {
                                        if (Program.GetAilmentFromAilments("good calcum") != null) return;
                                        Console.WriteLine(
                                            "\nThe Skeltal Altar senses you haven't thanked Mr.Skeltal - it decides to punish you!");
                                        Program.AddAilment(new Ailment("bad calcum", "u didnt thnk mr.skeltal! sleep tight pupper.", AilmentType.DEBUFF,
                                            () =>
                                            {
                                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                                Console.WriteLine("\nSLEEP TIGHT PUPPER");
                                                Console.ForegroundColor = ConsoleColor.White;
                                            }, 
                                            () =>
                                            {
                                                Console.ForegroundColor = ConsoleColor.Red;
                                                Console.WriteLine("\nAll your bones shatter into a fine powder, thanks to your bad calcum!");
                                                Program.PlayerHealth = 0;
                                                Console.ForegroundColor = ConsoleColor.White;
                                            }, null, 2));
                                    },
                                    new Dictionary<string, Performance>
                                    {
                                        {
                                            "speak",
                                            () =>
                                            {
                                                Console.WriteLine(
                                                    "You walk over to the altar, and kneel to it.\n\nYou lock your hands together, and a strange force brings you to \nthank Mr. Skeltal.\n");

                                                Program.AddAilment(new Ailment("good calcum",
                                                    "u've thnkd mr.skeltal 2day. good calcum and helthy bnes 4ever!!1!",
                                                    AilmentType.BUFF,
                                                    () =>
                                                        Console.WriteLine(
                                                            "Mr. Skeltal's blessings of good calcum run through your veins, bringing you infinite happiness."),
                                                    () =>
                                                        Console.WriteLine(
                                                            "Somehow, you've lost favour with the skeletal deity, and your good calcum begins to fade."),
                                                    null, -1));
                                                return true;
                                            }
                                        }
                                    }
                                    )
                            })
                    },
                    {
                        new Tuple<int, int>(-3, -4),
                        new Room("Coal Processing Plant Lawn", "A large, greasy facility, with towering smokestacks.\n\nTo the East is a dirt road.\nTo the North is a quad-coloured door.",
                            (int)RoomExits.NORTH | (int)RoomExits.EAST, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Door", "A quad-coloured door: red, blue, green, and yellow.", null,null,new Dictionary<string, Performance>
                                {
                                    {
                                        "attacked",
                                        () =>
                                        {
                                            Console.WriteLine("\nYou don't even bother attacking this door - it's far too large to break down.");
                                            return true;
                                        }
                                    },
                                    {
                                        "go_north",
                                        () =>
                                        {
                                            if (Program.PlayerHasItem("red house key") && Program.PlayerHasItem("blue house key") &&
                                                Program.PlayerHasItem("green house key") && Program.PlayerHasItem("yellow house key"))
                                            {
                                                Console.WriteLine("\nYou put all four keys into their respective slots.");
                                                Console.WriteLine("\nThe door creaks open, taking a little while to fully swing.");
                                                return true;
                                            }
                                            Console.WriteLine("\nThe door won't seem to budge without all four coloured keys...");
                                            return false;
                                        }
                                    }
                                })
                            })
                    },
                    {
                        new Tuple<int, int>(-3, -3),
                        new Room("Coal Processing Plant Lobby", "A drab, beige room, with chairs and a desk.\n\nTo the West is a set coal coke ovens, and to the North is a break room.\nTo the South there's a portal to the outside world.",
                            (int)RoomExits.NORTH|(int)RoomExits.SOUTH|(int)RoomExits.WEST, RoomType.DECORATIVE)
                    },
                    {
                        new Tuple<int, int>(-3, -2),
                        new Room("Coal Processing Plant Break Room", "A fairly ugly baby blue room, with a big table with chairs surrounding it.\n\nTo the South lies the lobby of the facility.",
                            (int)RoomExits.SOUTH, RoomType.DECORATIVE, new List<Item>{new Item("Metal Bowl", "A sturdy bowl, made out of steel.", ItemType.INGREDIENT)})
                    },
                    {
                        new Tuple<int, int>(-4, -3),
                        new Room("Coal Processing Plant Ovens", "A series of coke ovens line the walls, put up on huge stilts.\n\nTo the East lies the lobby of the facility.",
                            (int)RoomExits.EAST, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Oven","An oven for coal coke.", () => Console.WriteLine("One oven catches your eye in particular."), null, new Dictionary<string, Performance>
                                {
                                    {
                                        "attacked",
                                        () =>
                                        {
                                            if (Program.PlayerHasItem("bowl of cake mixture"))
                                            {
                                                Console.WriteLine("\nYou thrust your bowl of cake mix into the fire, almost burning your hands.\n\nAfter a few minutes, it's fully baked.");
                                                Program.RemoveItem("bowl of cake mixture");
                                                Program.PlayerInventory.Add(new Item("Cake", "A delicious spongecake. Needs something to rest on...", ItemType.SPECIAL, () =>Console.WriteLine("\nWhy would you eat her cake, you monster?")));
                                                return true;
                                            }
                                            Console.WriteLine("\nYou savagely beat the top of the oven, to no avail.");
                                            return true;
                                        }
                                    }
                                })
                            })
                    },
                    {
                        new Tuple<int, int>(-2, -10),
                        new Room("Her Lawn", "The lawn is trimmed to perfection, and decorated with cute gnomes.\n\nTo the North is a dirt road.\nTo the South is her door.",
                            (int)RoomExits.NORTH | (int)RoomExits.SOUTH, RoomType.DECORATIVE, new List<Npc>
                            {
                                new Npc("Door", "A reinforced metal door.", null, null, new Dictionary<string, Performance>
                                {
                                    {
                                        "go_south",
                                        () =>
                                        {
                                            if (Program.PlayerHasItem("cake on a plate"))
                                            {
                                                return true;
                                            }
                                            if (Program.PlayerHasItem("cake"))
                                            {
                                                Console.WriteLine("\nMaybe you should find something to put the cake on...");
                                                return false;
                                            }
                                            Console.WriteLine("\nYou're not gonna walk in there without her gift!");
                                            return false;
                                        }
                                    }
                                })
                            })
                    },
                    {
                        new Tuple<int, int>(-2, -11),
                        new Room("Her House", "", 0, RoomType.ENDING)
                    },    
                    {
                        new Tuple<int, int>(7, 18),
                        new Room("The Hoofi Room", "A room, emenating an intense, indescribable beauty.\nLooks like the perfect place to use a certain glass trinket....", 0,
                            RoomType.HOOFI)
                    }
                };
        }
    }
}
