﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HoofiGame
{
    internal class ZopliconeMixer : TextWriter
    {
        public TextWriter OriginalOut;

        public ZopliconeMixer()
        {
            OriginalOut = Console.Out;
        }

        public override Encoding Encoding
        {
            get { return new ASCIIEncoding(); }
        }

        public string RandomizeString(string message)
        {
            var shuffled = new List<Tuple<string, char>>();
            var shuffle = "";
            foreach (var c in message)
            {
                if (Char.IsWhiteSpace(c))
                {
                    shuffled.Add(new Tuple<string, char>(shuffle, c));
                    shuffle = "";
                }
                else shuffle += c;
            }
            shuffled.Add(new Tuple<string, char>(shuffle, ' '));


            var finalString = "";
            foreach (var str in shuffled)
            {
                finalString +=
                    new string(str.Item1.ToCharArray().OrderBy(s => (Program.Random.Next(2)%2) == 0).ToArray());
                finalString += str.Item2;
            }
            return finalString;
        }

        public override void Write(string message)
        {
            OriginalOut.Write(RandomizeString(message));
        }

        public override void WriteLine(string message)
        {
            OriginalOut.WriteLine(RandomizeString(message));
        }
    }

    internal class Program
    {
        public static string PlayerName = "Ethan";
        public static Dictionary<Tuple<int, int>, Room> RoomList;
        public static int PlayerHealth = 10;
        public static List<Item> PlayerInventory = new List<Item>();
        public static List<Ailment> PlayerStatuses = new List<Ailment>();
        public static Tuple<int, int> PlayerPosition = new Tuple<int, int>(0, 0);

        public static Dictionary<Tuple<string, string>, Item> Combinations = new Dictionary<Tuple<string, string>, Item>
        {
            {
                new Tuple<string, string>("milk jug", "stick of butter"),
                new Item("Milk/Butter Mixture", "A concoction of milk and butter.", ItemType.MIXTURE)
            },
            {
                new Tuple<string, string>("egg carton", "milk/butter mixture"),
                new Item("Milk/Butter/Egg Mixture", "A concotion of milk, butter, and eggs.", ItemType.MIXTURE)
            },
            {
                new Tuple<string, string>("flour bag", "salt shaker"),
                new Item("Flour/Salt Mixture", "A concoction of flour and salt.", ItemType.MIXTURE)
            },
            {
                new Tuple<string, string>("baking powder can", "flour/salt mixture"),
                new Item("Flour/Salt/Powder Mixture", "A concoction of flour, salt, and baking powder.", ItemType.MIXTURE)
            },
            {
                new Tuple<string, string>("flour/salt/powder mixture", "milk/butter/egg mixture"),
                new Item("Cake Base", "An unflavoured base for a cake.", ItemType.MIXTURE)
            },
            {
                new Tuple<string, string>("cake base","vanilla extract"),
                new Item("Flavoured Cake Mixture", "A flavoured cake mixture. Vanilla!", ItemType.MIXTURE)
            },
            {
                new Tuple<string, string>("flavoured cake mixture", "metal bowl"), 
                new Item("Bowl of Cake Mixture", "A bowl full of delicious cake mix. Vanilla!", ItemType.SPECIAL)
            },
            {
                new Tuple<string, string>("cake", "plate"),
                new Item("Cake on a Plate", "I want these motherfucking cakes off this motherfucking plate!", ItemType.CAKE)
            }
        };

        public static List<string> NegativesList = new List<string>
        {
            "Not quite sure what you mean.",
            "Uhhhhh...",
            "Parlez-vous anglais?",
            "Try that again, Buster.",
            "Don't use that type of language with me!",
            "I can't quite...uh...what?",
            "Yup. Mhmm. Totally got that."
        };

        public static List<string> WallList = new List<string>
        {
            "That's a wall, Buster Brown.",
            "I mean, you can keep sexing that wall...I guess.",
            "Try that one again.",
            "Running into that wall can't feel too good.",
            "No exit!...in this direction."
        };

        public static Random Random = new Random();
        public static bool DisplaySurroundings = true;
        public static TextWriter StandardOutput = Console.Out;
        public static bool Exit = false;

        private static void Main()
        {
            while (!Exit)
            {
                PlayerHealth = 10;
                PlayerInventory = new List<Item>();
                PlayerStatuses = new List<Ailment>();
                PlayerPosition = new Tuple<int, int>(-2, -10);
                DisplaySurroundings = true;
                PlayerName = "Ethan";
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\t\t\t\tTHE PERFECT GIFT");
                Console.WriteLine("\t\tA (horribly rushed and probably mediocre) present.");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\n\n\n\nWhat is your name, hero? (Just press enter for the worst name in existance.)");
                Console.Write(">>> ");
                var pn = Console.ReadLine();
                if (string.IsNullOrEmpty(pn)) Console.WriteLine("\nReally? Oooook....Ethan it is!");
                else
                    switch (pn.ToLower())
                    {
                        case "christie":
                        case "hoofi":
                            PlayerName = pn;
                            Console.WriteLine("\n"+ PlayerName + "? That's a beautiful name. Take this!");
                            Console.Write("The narrator gives you a ");
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write("glass heart");
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine(".");
                            PlayerInventory.Add(new Item("Glass Heart",
                                "A very special and intricate glass heart - signed with the initals E.B.",
                                ItemType.SPECIAL, () =>
                                {
                                    if (PlayerPosition.Item1 == 7 && PlayerPosition.Item2 == 18)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Cyan;
                                        Console.WriteLine("\nThe heart begins to glow, bathed in an effervescent cyan light.");
                                        Console.WriteLine("\nSuddenly, a voice fills your head...");
                                        PressAnyKey();
                                        Console.ForegroundColor = ConsoleColor.White;
                                        Console.WriteLine("Dear Christie,");
                                        Console.WriteLine("\nIt's your birthday." +
                                                          "\nIt's a time of celebration and merriment, but also a time of reflection." +
                                                          "\nYou're twenty years old - a milestone in and of itself." +
                                                          "\nTwo decades have passed by, in which so much has changed, including you." +
                                                          "\n\nI've only known you for about a month - I can say without hesitation" +
                                                          "\nthat I'm sorry I didn't meet you sooner. It's been such a ride getting" +
                                                          "\nto know everyone in the Turtle Tuesday group, but one person in particular" +
                                                          "\nhas this peculiar ability to put the dumbest grin on my stupid face -" +
                                                          "\nand that's you." +
                                                          "\n\nThis game was a labour of my imagination - a stupid little idea" +
                                                          "\nfor something to give you that popped into my head at 5 in the morning" +
                                                          "\none night after you fell asleep. I'm sorry if it's not up to par, or" +
                                                          "\nnot terribly interesting - a lot of it is supposed to be ironic and meta," +
                                                          "\nand I'm pretty bad at *both* of those things. Nevertheless, I made it for" +
                                                          "\nyou, and solely for you.");
                                        Console.ForegroundColor = ConsoleColor.DarkGray;
                                        Console.WriteLine("\nPress Any Key To Move To Page 2...");
                                        Console.ForegroundColor = ConsoleColor.White;
                                        Console.ReadLine();
                                        Console.Clear();
                                        Console.WriteLine("Christie, for all that's changing in my own life right now," +
                                                          "\nyou're someone I'd like to remain a constant for a long time to come. I" +
                                                          "\nwant to get to know you better, I want to develop memories of you." +
                                                          "\nI've said before that I love you - like I love everyone in Turtle"+
                                                          "\nTuesday - simply because of how amazing and different everyone is." +
                                                          "\nBut Christie, when I say that I love you - I really do mean it. Every" +
                                                          "\nlittle quirk about you, every word you say, every time you laugh -" +
                                                          "\nthat's what makes me fall in love with you. As corny as it sounds" +
                                                          "\nreading it back to myself, it's the honest truth." +
                                                          "\n\nIt's only been a month, Christie. You've made me smile, you've made"+
                                                          "\nme laugh, and you've made me feel welcome. I can't help but imagine" +
                                                          "\nwhat the future holds for us." +
                                                          "\n\nI love you, and I hope that the ride never ends.");
                                        Console.WriteLine("\n\t\tWith Much Love,\n\t\t\tEthan");
                                        PressAnyKey();
                                        RollCredits();
                                        return;
                                    }
                                    Console.ForegroundColor = ConsoleColor.Blue;
                                    Console.WriteLine("\nThis isn't the right time, nor the place to use that. Be patient!");
                                }));
                            Console.WriteLine("\nYou'll get the secret ending with that :)");
                            break;
                        case "dirtycheaterman":
                            PlayerName = "{DEBUG}";
                            Console.WriteLine("\nHere's the warp whistle!");
                            Console.Write("The narrator gives you a ");
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write("warp whistle");
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine(".");
                            PlayerInventory.Add(new Item("Warp Whistle",
                                "A debug item for getting around to different rooms.", ItemType.WHISTLE,
                                () =>
                                {
                                    Console.WriteLine("\n*whistle sound*");
                                    Console.Write("\nPlease Type Your Requested X Coordinate: ");
                                    var x = Convert.ToInt32(Console.ReadLine());
                                    Console.Write("\nPlease Type Your Requested Y Coordinate: ");
                                    var y = Convert.ToInt32(Console.ReadLine());
                                    RoomChange(x, y, "");
                                    Console.WriteLine("\nTeleporting you to " + RoomList[PlayerPosition].Title + "\n");
                                }));
                            break;
                        default:
                            PlayerName = pn;
                            Console.WriteLine("\n"+PlayerName + "? That's a fine name!");
                            break;
                    }
                PressAnyKey();
                Console.WriteLine("LOADING!");
                RoomList = RoomInitalizer.InitalizeRooms(PlayerName);
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\t\t\t\t  BACKSTORY:\n");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(
                    "You wake up one fine morning, fresh as can be, and spring from your bed.\nThe birds are singing, the sun is shining - it's a beautiful day indeed.\nYou decide to take advantage of this miraculously beautiful day by...\n\nGetting on the computer.\nHmm. Anyway,\n\nLo and behold, your skype client is blown the fuck up, with a single message\ncatching your eye -\n\n\"DUDE, IT'S HER BIRTHDAY! YOU GOT HER SOMETHING, RIGHT?!\"");
                Console.WriteLine("\nFuck.");
                Console.WriteLine(
                    "\nDevoid of the gift, your task is clear: In the course of one day,\nFind a goddamn perfect gift for a goddamn perfect person.\nGood luck!");
                PressAnyKey();
                while (PlayerHealth > 0)
                {
#if !DEBUG
                    try
                    {
                        InterpretInput();
                    }
                    catch (Exception e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Error.WriteLine(
                            "You try to do that, but you end up falling into the never-ending void of unimplementation.");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Error.WriteLine("If you see the developer of this shitty game, tell him this: " +
                                                e.Message);
                        PressAnyKey();
                        DisplaySurroundings = true;
                    }
#else
                    InterpretInput();
#endif
                }
                if (RoomList[PlayerPosition].RoomType == RoomType.DEATH) RoomList[PlayerPosition].PrintRoom();
                Console.WriteLine("--------------------------------------------------------------------------------");
                Console.WriteLine("Well, you died.");
                Console.WriteLine("\nLooks like you'll have to try again!");
                PressAnyKey();
            }
        }

        public static void PressAnyKey()
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            StandardOutput.Write("\nPress any key to continue...");
            Console.ForegroundColor = ConsoleColor.White;
            Console.ReadKey();
            Console.Clear();
        }

        private static void InterpretInput()
        {
            if (DisplaySurroundings)
            {
#if DEBUG
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.WriteLine("[" + PlayerPosition.Item1 + "," + PlayerPosition.Item2 + "]");
                Console.ForegroundColor = ConsoleColor.White;
#endif
                RoomList[PlayerPosition].PrintRoom();
                DisplaySurroundings = false;
            }
            Console.ForegroundColor = ConsoleColor.Blue;
            StandardOutput.Write("\n>>> ");
            Console.ForegroundColor = ConsoleColor.Gray;
            var input = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;
            input = input == null ? "" : input.ToLower();
            if (input.StartsWith("view ")) ViewItem(input.Replace("view ", ""));
            else if (input.StartsWith("take ")) TakeItem(input.Replace("take ", ""));
            else if (input.StartsWith("use ")) UseItem(input.Replace("use ", ""));
            else if (input.StartsWith("ailment ")) ExplainAilment(input.Replace("ailment ", ""));
            else if (input.StartsWith("talk ")) TalkToNpc(input.Replace("talk ", ""));
            else if (input.StartsWith("lookat ")) LookAtNpc(input.Replace("lookat ", ""));
            else if (input.StartsWith("attack ")) AttackNpc(input.Replace("attack ", ""));
            else if (input.StartsWith("combine "))
            {
                var split = input.Replace("combine ", "").Split(new[] {','});
                if (split.Length != 2) Console.WriteLine("\nToo many or too few items to combine!");
                else CombineItems(split[0],split[1]);
            }
            else
            {
                switch (input)
                {
                    case "exit":
                        EndTheGame();
                        break;
                    case "n":
                    case "north":
                        if ((RoomList[PlayerPosition].ExitMask & (int) RoomExits.NORTH) == (int) RoomExits.NORTH)
                            RoomChange(PlayerPosition.Item1, PlayerPosition.Item2 + 1, "go_north");
                        else Console.WriteLine(WallList[Random.Next(WallList.Count)]);
                        break;
                    case "s":
                    case "south":
                        if ((RoomList[PlayerPosition].ExitMask & (int) RoomExits.SOUTH) == (int) RoomExits.SOUTH)
                            RoomChange(PlayerPosition.Item1, PlayerPosition.Item2 - 1, "go_south");
                        else Console.WriteLine(WallList[Random.Next(WallList.Count)]);
                        break;
                    case "w":
                    case "west":
                        if ((RoomList[PlayerPosition].ExitMask & (int) RoomExits.WEST) == (int) RoomExits.WEST)
                            RoomChange(PlayerPosition.Item1 - 1, PlayerPosition.Item2, "go_west");
                        else Console.WriteLine(WallList[Random.Next(WallList.Count)]);
                        break;
                    case "e":
                    case "east":
                        if ((RoomList[PlayerPosition].ExitMask & (int) RoomExits.EAST) == (int) RoomExits.EAST)
                            RoomChange(PlayerPosition.Item1 + 1, PlayerPosition.Item2, "go_east");
                        else Console.WriteLine(WallList[Random.Next(WallList.Count)]);
                        break;
                    case "look":
                        DisplaySurroundings = true;
                        break;
                    case "inventory":
                    case "inv":
                        ShowInventory();
                        break;
                    case "status":
                    case "stat":
                        ShowStatus();
                        break;
                    case "help":
                        ShowHelp();
                        break;
                    default:
                        Console.WriteLine("\n" + NegativesList[Random.Next(NegativesList.Count)]);
                        break;
                }
            }
            AilmentTick();
        }


        private static void AilmentTick()
        {
            List<Ailment> toRemove = null;
            foreach (var ailment in PlayerStatuses)
            {
                if (ailment.TicksActive != -1) ailment.TicksActive--;
                if (ailment.TicksActive > 0 || ailment.TicksActive == -1) continue;
                if (toRemove == null) toRemove = new List<Ailment>();
                toRemove.Add(ailment);
            }
            if (toRemove == null) return;
            foreach (var removal in toRemove) RemoveAilment(removal);
        }

        public static void RemoveAilment(Ailment ailment)
        {
            ailment.Offset();
            ailment.PrintAilment(false, false);
            Console.WriteLine(" has worn off!");
            PlayerStatuses.Remove(ailment);
        }

        private static void RoomChange(int x, int y, string performance)
        {
            if (!RoomList.ContainsKey(new Tuple<int, int>(x, y)))
                throw new Exception("Change to X: " + x + ",Y: " + y + " wasn't implemented!");
            var canGo = true;
            if (RoomList[PlayerPosition].NpcList != null)
            {
// ReSharper disable once AccessToModifiedClosure
                foreach (
                    var npc in
                        RoomList[PlayerPosition].NpcList.Where(npc => npc.Performances != null && npc.Performances.ContainsKey(performance) && canGo)
                    )
                {
                    canGo = npc.Performances[performance]();
                }
            }
            if (!canGo) return;
            RoomList[PlayerPosition].OnExit();
            PlayerPosition = new Tuple<int, int>(x, y);
            DisplaySurroundings = true;
            var room = RoomList[PlayerPosition];
            switch (room.RoomType)
            {
                case RoomType.DEATH:
                    PlayerHealth = 0;
                    break;
                case RoomType.ENDING:
                    DisplaySurroundings = false;
                    DisplayEnding();
                    break;
            }
        }

        public static void ShowInventory()
        {
            if (PlayerInventory.Count == 0)
                Console.WriteLine("Your backpack is completely empty.\n\nTry picking something up!");
            else
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Inventory:");
                Console.ForegroundColor = ConsoleColor.White;
                foreach (var item in PlayerInventory)
                {
                    item.PrintItemName();
                }
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void ShowStatus()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Health: ");
            Console.ForegroundColor = PlayerHealth >= 7
                ? ConsoleColor.Green
                : PlayerHealth >= 4 ? ConsoleColor.Yellow : PlayerHealth >= 2 ? ConsoleColor.Red : ConsoleColor.DarkRed;
            Console.WriteLine(PlayerHealth);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Status Ailments: ");
            foreach (var ailment in PlayerStatuses) ailment.PrintAilment(false);
            Console.ForegroundColor = ConsoleColor.White;
            if (PlayerStatuses.Count == 0) Console.WriteLine("There's nothing here!");
        }

        public static void CombineItems(string itemOne, string itemTwo)
        {
            if (!PlayerHasItem(itemOne.ToLower()) || !PlayerHasItem(itemTwo.ToLower()))
            {
                Console.WriteLine("\nYou don't have one or both of those items!");
                return;
            }
            if (Combinations.ContainsKey(new Tuple<string, string>(itemOne.ToLower(), itemTwo.ToLower())))
            {
                RemoveItem(itemOne.ToLower());
                RemoveItem(itemTwo.ToLower());
                Console.Write("\nThe two items combine into ");
                Combinations[new Tuple<string, string>(itemOne.ToLower(), itemTwo.ToLower())].PrintItemName(false);
                Console.WriteLine("!");
                PlayerInventory.Add(Combinations[new Tuple<string, string>(itemOne.ToLower(), itemTwo.ToLower())]);
                return;
            }
            if (Combinations.ContainsKey(new Tuple<string, string>(itemTwo.ToLower(), itemOne.ToLower())))
            {
                RemoveItem(itemOne.ToLower());
                RemoveItem(itemTwo.ToLower());
                Console.Write("\nThe two items combine into ");
                Combinations[new Tuple<string, string>(itemTwo.ToLower(), itemOne.ToLower())].PrintItemName(false);
                Console.WriteLine("!");
                PlayerInventory.Add(Combinations[new Tuple<string, string>(itemTwo.ToLower(), itemOne.ToLower())]);
                return;
            }
            Console.WriteLine("\nThose items don't combine into anything!");
        }

        public static void ExplainAilment(string ail)
        {
            foreach (var ailment in PlayerStatuses.Where(ailment => ailment.Name.ToLower() == ail))
            {
                ailment.PrintAilment(true);
                return;
            }
            Console.WriteLine("You don't have an ailment with that name.");
        }

        public static void ShowHelp()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("NORTH or N to travel North.");
            Console.WriteLine("SOUTH or S to travel South.");
            Console.WriteLine("WEST or W to travel West.");
            Console.WriteLine("EAST or E to travel East.");
            Console.WriteLine("LOOK to check your surroundings again.");
            Console.WriteLine("INVENTORY or INV to bring up your currently held items.");
            Console.WriteLine("STATUS or STAT to check out your status.");
            Console.WriteLine("TAKE to take an item from the ground.");
            Console.WriteLine("VIEW <item name> to view the specifics of an item in your inventory.");
            Console.WriteLine("USE <item name> to use a consumable item in your inventory.");
            Console.WriteLine("AILMENT <ailment name> to view the specifics of an ailment you have.");
            Console.WriteLine("TALK <npc name> to talk to an NPC in the current area.");
            Console.WriteLine("ATTACK <name> to beat something up. Great way to open things.");
            Console.WriteLine("COMBINE <item one>,<item two> combines two items.");
            Console.WriteLine("LOOKAT <name> to look at something/someone.");
            Console.WriteLine("HELP to bring up this help menu.");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void UseItem(string item)
        {
            foreach (var itemUse in PlayerInventory.Where(itemUse => itemUse.Name.ToLower() == item))
            {
                if (itemUse.ConsumeAction != null)
                {
                    itemUse.ConsumeAction();
                    return;
                }
                Console.WriteLine("You don't know what to do with it!");
                return;
            }
            Console.WriteLine("You don't have " + item + " in your inventory!");
        }

        public static void EndTheGame()
        {
            Environment.Exit(0);
        }
        public static void TakeItem(string item)
        {
            foreach (var itemDrop in RoomList[PlayerPosition].Items.Where(itemDrop => itemDrop.Name.ToLower() == item))
            {
                Console.Write("You pick up the ");
                itemDrop.PrintItemName(false);
                Console.WriteLine(" and put " + (itemDrop.Name.EndsWith("s") ? "them " : "it ") + "into your inventory.");
                PlayerInventory.Add(itemDrop);
                RoomList[PlayerPosition].PickupItem(itemDrop);
                return;
            }
            Console.WriteLine("That's not an item...at all.");
        }

        public static void AddItemToRoom(Item item, Tuple<int, int> pos)
        {
            if(!RoomList.ContainsKey(pos)) throw new Exception("Room [" + pos.Item1 + "," + pos.Item2 + "] does not exist!");
            RoomList[pos].Items.Add(item);
        }

        public static void ViewItem(string item)
        {
            foreach (var itemInstance in PlayerInventory.Where(itemInstance => itemInstance.Name.ToLower() == item))
            {
                itemInstance.PrintItemName();
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(itemInstance.Description);
                return;
            }
            Console.WriteLine("Sorry, don't think you have that item.");
        }

        public static void TalkToNpc(string npc)
        {
            foreach (
                var npcActive in RoomList[PlayerPosition].NpcList.Where(npcActive => npcActive.Name.ToLower() == npc))
            {
                if (npcActive.Performances == null || !npcActive.Performances.ContainsKey("speak"))
                    Console.WriteLine("\nLooks like it won't talk back.");
                else npcActive.Performances["speak"]();
                return;
            }
            Console.WriteLine("No one by that name is here!");
        }

        public static bool PlayerHasItem(string itemName)
        {
            return PlayerInventory.Any(item => item.Name.ToLower() == itemName);
        }

        public static Item GetItemFromInventory(string itemName)
        {
            return !PlayerHasItem(itemName)
                ? null
                : PlayerInventory.First(item => item.Name.ToLower() == itemName);
        }

        public static bool PlayerHasAilment(string ailmentName)
        {
            return PlayerStatuses.Any(ailment => ailment.Name.ToLower() == ailmentName);
        }

        public static bool RoomHasNpc(string npcName, Tuple<int, int> pos)
        {
            return RoomList[pos].NpcList.Any(npc => npc.Name.ToLower() == npcName);
        }

        public static Npc GetNpc(string npcName, Tuple<int, int> pos)
        {
            return !RoomHasNpc(npcName, pos)
                ? null
                : RoomList[PlayerPosition].NpcList.First(npc => npc.Name.ToLower() == npcName);
        }

        public static Ailment GetAilmentFromAilments(string ailmentName)
        {
            return !PlayerHasAilment(ailmentName)
                ? null
                : PlayerStatuses.First(ailment => ailment.Name.ToLower() == ailmentName);
        }

        public static void LookAtNpc(string npc)
        {
            foreach (
                var npcActive in RoomList[PlayerPosition].NpcList.Where(npcActive => npcActive.Name.ToLower() == npc))
            {
                Console.WriteLine(npcActive.Description);
                return;
            }
            Console.WriteLine("No one by that name is here!");
        }

        public static void RemoveItem(string item)
        {
            var itemBuff = PlayerInventory.FirstOrDefault(itemRem => itemRem.Name.ToLower() == item);
            if (itemBuff != null) PlayerInventory.Remove(itemBuff);
        }

        public static void AttackNpc(string npc)
        {
            foreach (
                var npcActive in RoomList[PlayerPosition].NpcList.Where(npcActive => npcActive.Name.ToLower() == npc))
            {
                if (npcActive.Performances == null || !npcActive.Performances.ContainsKey("attacked"))
                    Console.WriteLine("\nWhy would you do that? They're not harming you.\n");
                else npcActive.Performances["attacked"]();
                return;
            }
            Console.WriteLine("No one by that name is here!");
        }

        public static void DisplayEnding()
        {
            Console.Clear();
            Console.WriteLine("\nYou've done it - you've gotten her a gift.\n\nIt's a pretty lackluster cake, but it's from the heart.");
            if (PlayerName.ToLower() == "hoofi" || PlayerName.ToLower() == "christie")
            {
                Console.WriteLine("\nAs you hand the cake to her, you suddenly realize - she is you!");
                Console.WriteLine("\nA paradox opens up, and swallows you whole.");
                RoomChange(7, 18, "");
                PressAnyKey();
            }
            else
            {
                Console.WriteLine("\nAs you hand the cake to her, she swats it out of your hand.");
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("\nWOAH WOAH WOAH!");
                Console.WriteLine("WHAT THE FUCK IS THIS?");
                Console.WriteLine("FUCKIN' SPONGECAKE?");
                Console.WriteLine("...FUCK!");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\n...It's the thought that counts.");
                Console.WriteLine("\n\nTHANK YOU FOR PLAYING!");
                PressAnyKey();
                RollCredits();
            }
        }

        public static void RollCredits()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\t\t\t\t     CREDITS:");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\t\t\tETHAN - PROGRAMMING,IDEAS,PLANNING");
            Console.WriteLine("\n\t\t\t     DUSTIN - IDEAS,PLANNING");
            Console.WriteLine("\n\t\t\t      JOHN - IDEAS,PLANNING");
            PressAnyKey();
            EndTheGame();
        }
        public static void AddAilment(Ailment ailment)
        {
            ailment.Onset();
            PlayerStatuses.Add(ailment);
        }
    }
}