﻿using System.Collections.Generic;

namespace HoofiGame
{
    public delegate void NpcEntryAction();

    public delegate void NpcExitAction();

    public delegate bool Performance();
    public class Npc
    {
        public string Name;
        public string Description;
        public NpcEntryAction OnEnter;
        public NpcExitAction OnExit;
        public Dictionary<string, Performance> Performances;
        public int Health = 10;

        public Npc(string name, string description, NpcEntryAction entryAction = null, NpcExitAction exitAction = null, Dictionary<string, Performance> performances = null)
        {
            Name = name;
            Description = description;
            OnEnter = entryAction;
            OnExit = exitAction;
            if (performances == null) Performances = null;
            else
            {
                Performances = new Dictionary<string, Performance>();
                foreach (var perf in performances)
                {
                    Performances[perf.Key] = perf.Value;
                }
            }
        }
    }
}
