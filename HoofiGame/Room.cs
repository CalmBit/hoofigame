﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HoofiGame
{
    public enum RoomExits
    {
        NORTH = 1,
        SOUTH = 2,
        WEST = 4,
        EAST = 8
    }

    public enum RoomType
    {
        PATH,
        DECORATIVE,
        ACTION,
        DEATH,
        HOOFI,
        ENDING
    }
    public class Room
    {
        public string Title;
        public string Description;
        public int ExitMask;
        public RoomType RoomType;
        public List<Item> Items;
        public List<Npc> NpcList;
        public Room(string title, string description, int exitMask, RoomType roomType, List<Item> itemList, List<Npc> npcList)
        {
            Title = title;
            Description = description;
            ExitMask = exitMask;
            RoomType = roomType;
            Items = new List<Item>();
            foreach (var item in itemList)
            {
                Items.Add(item);
            }
            NpcList = new List<Npc>();
            foreach (var npc in npcList)
            {
                NpcList.Add(npc);
            }
        }

        public Room(string title, string description, int exitMask, RoomType roomType)
            : this(title, description, exitMask, roomType, new List<Item>(), new List<Npc>())
        {
            
        }

        public Room(string title, string description, int exitMask, RoomType roomType, List<Item> itemList)
            : this(title, description, exitMask, roomType, itemList, new List<Npc>())
        {

        }

        public Room(string title, string description, int exitMask, RoomType roomType, List<Npc> npcList)
            : this(title, description, exitMask, roomType, new List<Item>(), npcList)
        {
            
        }
        public void PickupItem(Item item)
        {
            Items.Remove(item);
        }

        public void OnEnter()
        {
            foreach (var npc in NpcList.Where(npc => npc.OnEnter != null))
            {
                npc.OnEnter();
            }
        }

        public void OnExit()
        {
            foreach (var npc in NpcList.Where(npc => npc.OnExit != null))
            {
                npc.OnExit();
            }
        }

        public void PrintRoom()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Title + "\n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(Description + "\n");
            OnEnter();
            if (Items.Count == 0) return;
            foreach (var item in Items)
            {
                Console.Write("There");
                Console.Write(item.Name.EndsWith("s") ? " are " : " is a ");
                item.PrintItemName(false);
                Console.WriteLine(" on the ground here.");
            }
            /* if (RoomType == RoomType.DEATH) return;
            //Room has plural exits?
            if (ExitMask != 1 && ExitMask != 2 && ExitMask != 4 && ExitMask != 8) Console.Write("Exits: ");
            else Console.Write("Exit: ");
            if ((ExitMask & (int) RoomExits.NORTH) == (int) RoomExits.NORTH)
            {
                Console.Write("North ");
            }
            if ((ExitMask & (int) RoomExits.SOUTH) == (int) RoomExits.SOUTH)
            {
                Console.Write("South ");
            }
            if ((ExitMask & (int) RoomExits.WEST) == (int) RoomExits.WEST)
            {
                Console.Write("West ");
            }
            if ((ExitMask & (int) RoomExits.EAST) == (int) RoomExits.EAST)
            {
                Console.Write("East ");
            }
            Console.WriteLine("");*/
        }
    }
}
