﻿using System;

namespace HoofiGame
{
    public enum ItemType
    {
        KEY,
        SPECIAL,
        WHISTLE,
        CONSUMABLE,
        TAPE,
        INGREDIENT,
        EASTER,
        MIXTURE,
        CAKE
    }

    public delegate void ConsumableAction();

    public class Item
    {
        public string Name;
        public string Description;
        public ItemType ItemType;
        public ConsumableAction ConsumeAction;

        public Item(string name, string description, ItemType itemType)
        {
            Name = name;
            Description = description;
            ItemType = itemType;
        }

        public Item(string name, string description, ItemType itemType, ConsumableAction consumeAction)
            : this(name, description, itemType)
        {
            ConsumeAction = consumeAction;
        }

        public Item(string name, string description, ConsumableAction consumeAction)
            : this(name, description, ItemType.CONSUMABLE, consumeAction)
        {
            
        }



        public void PrintItemName(bool formatting=true)
        {
            Console.ForegroundColor = ConsoleColor.White;
            if(formatting) Console.Write("- ");
            switch (ItemType)
            {
                case ItemType.KEY:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case ItemType.SPECIAL:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;
                case ItemType.INGREDIENT:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case ItemType.WHISTLE:
                case ItemType.TAPE:
                case ItemType.EASTER:
                case ItemType.CAKE:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    break;
                case ItemType.MIXTURE:
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    break;
            }
            if(formatting) Console.WriteLine(Name);
            else Console.Write(Name);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
